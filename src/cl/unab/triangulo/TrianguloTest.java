package cl.unab.triangulo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TrianguloTest {
//Pruebas para comprobar si son del tipo triangulo 
	@Test
	public void testIsoscelesa() {
		TipoTriangulos triangulo = new TipoTriangulos(10,10,19);
		assertEquals("isosceles",triangulo.tipoTriangulos());		
	}
	
	@Test
	public void testIsoscelesb() {
		TipoTriangulos triangulo = new TipoTriangulos(19,10,10);
		assertEquals("isosceles",triangulo.tipoTriangulos());
				
	}
	
	@Test
	public void testIsoscelesc() {
		TipoTriangulos triangulo = new TipoTriangulos(10,19,10);
		assertEquals("isosceles",triangulo.tipoTriangulos());
				
	}

	@Test
	public void testEquilatero() {
		TipoTriangulos triangulo = new TipoTriangulos(5,5,5);
		assertEquals("equilatero",triangulo.tipoTriangulos());
				
	}

	@Test
	public void testEscaleno() {
		TipoTriangulos triangulo = new TipoTriangulos(1,2,3);
		assertEquals("escaleno",triangulo.tipoTriangulos());
				
	}
	
	
// Prueba de falla tipo de triangulos
	//Isoceles
	@Test
	public void testfailIsoscelesa() {
		TipoTriangulos triangulo = new TipoTriangulos(4,10,19);
		assertEquals("isosceles",triangulo.tipoTriangulos());
				
	}
	@Test
	public void testfailIsoscelesb() {
		TipoTriangulos triangulo = new TipoTriangulos(5,5,5);
		assertEquals("isosceles",triangulo.tipoTriangulos());
				
	}
	@Test
	public void testfailIsoscelesc() {
		TipoTriangulos triangulo = new TipoTriangulos(3,19,10);
		assertEquals("isosceles",triangulo.tipoTriangulos());
				
	}
	
	//Equilatero
	@Test
	public void testfailEquilateroa() {
		TipoTriangulos triangulo = new TipoTriangulos(5,4,5);
		assertEquals("equilatero",triangulo.tipoTriangulos());
	}
	@Test
	public void testfailEquilaterob() {
		TipoTriangulos triangulo = new TipoTriangulos(9,4,5);
		assertEquals("equilatero",triangulo.tipoTriangulos());
	}
	
	//Escaleno
	@Test
	public void testfailEscalenoa() {
		TipoTriangulos triangulo = new TipoTriangulos(1,1,3);
		assertEquals("escaleno",triangulo.tipoTriangulos());
				
	}
	public void testfailEscalenob() {
		TipoTriangulos triangulo = new TipoTriangulos(1,1,1);
		assertEquals("escaleno",triangulo.tipoTriangulos());
				
	}

//Prueba triangulos falsos
	@Test
	public void testTrianguloa() {
		TipoTriangulos triangulo = new TipoTriangulos(10,10,21);
		assertEquals(false, triangulo.soyTriangulo());
				
	}
	@Test
	public void testTriangulob() {
		TipoTriangulos triangulo = new TipoTriangulos(21,10,10);
		assertEquals(false,triangulo.soyTriangulo());
				
	}
	@Test
	public void testTrianguloc() {
		TipoTriangulos triangulo = new TipoTriangulos(10,21,10);
		assertEquals(false,triangulo.soyTriangulo());
				
	}

//Prueba falla triangulos 
	@Test
	public void testfailTrianguloa() {
		TipoTriangulos triangulo = new TipoTriangulos(13,10,21);
		assertEquals(false, triangulo.soyTriangulo());
					
	}
	@Test
	public void testfailTriangulob() {
		TipoTriangulos triangulo = new TipoTriangulos(21,16,11);
		assertEquals(false,triangulo.soyTriangulo());
					
	}
	@Test
	public void testfailTrianguloc() {
		TipoTriangulos triangulo = new TipoTriangulos(14,21,10);
		assertEquals(false,triangulo.soyTriangulo());
					
	}

//Prueba error triangulos 
	@Test
	public void testerrorTriangulob() {
		TipoTriangulos triangulo = new TipoTriangulos("a","b",5);
		assertEquals(false,triangulo.soyTriangulo());
					
	}
	//Prueba de clase
	@Test
	public void testerrorTrianguloc() {
		ClaseTriangulos triangulo;
					
	}



















}
