/**
 * 
 */
/**
 * @author DjTeddy
 *
 */
package cl.unab.triangulo;


public class TipoTriangulos{
	
	private int lado1;
	private int lado2;
	private int lado3;
	
	public TipoTriangulos(int lado1, int lado2, int lado3) {
		super();
		this.lado1 = lado1;
		this.lado2 = lado2;
		this.lado3 = lado3;
	}
	
	public String tipoTriangulos() {
		if(lado1 == lado2 && lado2 == lado3) {
			return("equilatero");
		} else if(lado1 != lado2 && lado1 != lado3 && lado2 != lado3) {
			return("escaleno");
		} else return("isosceles");
	}
	
	public boolean soyTriangulo(){
		if(lado1+lado2 > lado3 && lado1+lado3 > lado2 && lado2+lado3 > lado1) {
			return true;
		}
		else {
			return false;
		}
	}
}